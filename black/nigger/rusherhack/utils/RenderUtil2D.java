package black.nigger.rusherhack.utils;

import static org.lwjgl.opengl.GL11.GL_LINE_LOOP;
import static org.lwjgl.opengl.GL11.GL_LINE_STRIP;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TRIANGLE_FAN;
import static org.lwjgl.opengl.GL11.glLineWidth;

import java.util.List;

import javax.vecmath.Point2d;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;

public class RenderUtil2D extends RenderUtil {

    public static void rectBordered(double x1, double y1, double x2, double y2, int fill, int outline, float width) {
        rect(x1, y1, x2, y2, fill, 1, GL_QUADS);
        rect(x1, y1, x2, y2, outline, width, GL_LINE_LOOP);
    }

    public static void rect(double x1, double y1, double x2, double y2, int color, float width, int renderCode) {
        float[] rgba = ColorUtil.getRGBA(color);
        preRender(rgba);

        glLineWidth((float) Math.max(0.1, width));

        Tessellator tess = Tessellator.getInstance();

        tess.draw();

        postRender();
    }

    public static void texturedRect(double x1, double y1, double x2, double y2, int color) {
        float[] rgba = ColorUtil.getRGBA(color);
        preRender(rgba);

        Tessellator tess = Tessellator.getInstance();

        GlStateManager.enableTexture2D();
        tess.draw();

        postRender();
    }

    public static void polygonBordered(List<Point2d> points, int fill, int outline, float width, boolean connected) {
        polygon(points, fill, 1, GL_TRIANGLE_FAN);
        polygon(points, outline, width, connected ? GL_LINE_LOOP : GL_LINE_STRIP);
    }

    public static void polygon(List<Point2d> points, int color, float width, int renderCode) {
        float[] rgba = ColorUtil.getRGBA(color);
        preRender(rgba);

        glLineWidth((float)Math.max(0.1, width));

        Tessellator tess = Tessellator.getInstance();

        postRender();
    }

    public static void circle(double x, double y, double radius, int color) {
        float[] rgba = ColorUtil.getRGBA(color);
        preRender(rgba);

        Tessellator tess = Tessellator.getInstance();

        for(double i = 0; i < 360; i++) {
            double cs = i*Math.PI/180;
            double ps = (i-1)*Math.PI/180;
            double[] outer = new double[] {
                    Math.cos(cs) * radius, -Math.sin(cs) * radius,
                    Math.cos(ps) * radius, -Math.sin(ps) * radius
            };

            tess.draw();
        }

        GlStateManager.disableBlend();
        GlStateManager.disableAlpha();
        GlStateManager.enableTexture2D();
        GlStateManager.alphaFunc(516, 0.1F);
    }

    public static void circleOutline(double x, double y, double radius, int color, float width) {
        float[] rgba = ColorUtil.getRGBA(color);
        preRender(rgba);

        glLineWidth((float) Math.max(0.1, width));

        Tessellator tess = Tessellator.getInstance();

        tess.draw();

        GlStateManager.disableBlend();
        GlStateManager.disableAlpha();
        GlStateManager.enableTexture2D();
        GlStateManager.alphaFunc(516, 0.1F);
    }

    public static void donut(double x, double y, double radius, double hole, int color) {
        float[] rgba = ColorUtil.getRGBA(color);
        preRender(rgba);

        Tessellator tess = Tessellator.getInstance();

        for(double i = 0; i < 360; i++) {
            double cs = i*Math.PI/180D;
            double ps = (i-1)*Math.PI/180D;
            double[] outer = new double[] { Math.cos(cs) * radius, -Math.sin(cs) * radius, Math.cos(ps) * radius, -Math.sin(ps) * radius };
            double[] inner = new double[] { Math.cos(cs) * hole, -Math.sin(cs) * hole, Math.cos(ps) * hole, -Math.sin(ps) * hole };

            tess.draw();
        }

        postRender();
    }

    public static void donutSeg(double x, double y, double radius, double hole, double part, int maxParts, double padding, int color) {
        float[] rgba = ColorUtil.getRGBA(color);
        preRender(rgba);

        Tessellator tess = Tessellator.getInstance();

        part = (part+maxParts)%maxParts;

        for(double i = (360D/maxParts)*(part+padding); i < ((360D/maxParts)*(part+1-padding))-0.5; i+= 1) {
            double cs = (-i)*Math.PI/180;
            double ps = (-i-1)*Math.PI/180;
            double[] outer = new double[] { Math.cos(cs) * radius, -Math.sin(cs) * radius, Math.cos(ps) * radius, -Math.sin(ps) * radius };
            double[] inner = new double[] { Math.cos(cs) * hole, -Math.sin(cs) * hole, Math.cos(ps) * hole, -Math.sin(ps) * hole };

            tess.draw();
        }

        postRender();
    }

    public static void donutSegFrac(double x, double y, double radius, double hole, double part, int maxParts, double frac, double padding, int color) {
        float[] rgba = ColorUtil.getRGBA(color);
        preRender(rgba);

        Tessellator tess = Tessellator.getInstance();

        part = (part+maxParts)%maxParts;

        for(double i = (360D/maxParts)*part; i < ((360D/maxParts)*(part+frac))-0.5; i++) {
            double cs = (-i)*Math.PI/180D;
            double ps = (-i-1)*Math.PI/180D;
            double[] outer = new double[] { Math.cos(cs) * radius, -Math.sin(cs) * radius, Math.cos(ps) * radius, -Math.sin(ps) * radius };
            double[] inner = new double[] { Math.cos(cs) * hole, -Math.sin(cs) * hole, Math.cos(ps) * hole, -Math.sin(ps) * hole };

            tess.draw();
        }

        postRender();
    }


}