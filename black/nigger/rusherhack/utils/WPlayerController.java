package black.nigger.rusherhack.utils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.inventory.ClickType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public final class WPlayerController extends Default {
    private static PlayerControllerMP getPlayerController() {
        return Minecraft.getMinecraft().playerController;
    }

    public static ItemStack windowClick_PICKUP(int slot) {
        return getPlayerController().windowClick(0, slot, 0, ClickType.PICKUP, mc.player);
    }

    public static ItemStack windowClick_QUICK_MOVE(int slot) {
        return getPlayerController().windowClick(0, slot, 0, ClickType.QUICK_MOVE, mc.player);
    }

    public static ItemStack windowClick_THROW(int slot) {
        return getPlayerController().windowClick(0, slot, 1, ClickType.THROW,
                mc.player);
    }

    public static void processRightClick() {
        getPlayerController().processRightClick(mc.player,
                mc.world, EnumHand.MAIN_HAND);
    }

    public static void processRightClickBlock(BlockPos pos, EnumFacing side, Vec3d hitVec) {
        getPlayerController().processRightClickBlock(mc.player,
                mc.world, pos, side, hitVec, EnumHand.MAIN_HAND);
    }
}