package black.nigger.rusherhack.utils;

import org.lwjgl.opengl.GL11;

public class RenderColor {
    public byte r;
    public byte g;
    public byte b;
    public byte a;
    public int rOrig;
    public int gOrig;
    public int bOrig;
    public int aOrig;

    public RenderColor(int r, int g, int b, int a) {
        this.r = (byte) Math.floorDiv(r, 2);
        this.g = (byte) Math.floorDiv(g, 2);
        this.b = (byte) Math.floorDiv(b, 2);
        this.a = (byte) Math.floorDiv(a, 2);
        this.rOrig = r;
        this.gOrig = g;
        this.bOrig = b;
        this.aOrig = a;
    }

    public static void glColor(int r, int g, int b) {
        glColor(r, g, b, 255);
    }

    public static void glColor(int r, int g, int b, int a) {
        GL11.glColor4b((byte) Math.floorDiv(r, 2), (byte) Math.floorDiv(g, 2), (byte) Math.floorDiv(b, 2), (byte) Math.floorDiv(a, 2));
    }

    public int getIntColor() {
        return (a & 255) << 24 | (r & 255) << 16 | (g & 255) << 8 | (b & 255) << 0;
    }
}
