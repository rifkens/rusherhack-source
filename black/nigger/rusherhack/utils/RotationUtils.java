package black.nigger.rusherhack.utils;

import com.stringer.annotations.HideAccess;
import com.stringer.annotations.StringEncryption;

import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

@HideAccess
@StringEncryption
public class RotationUtils {
	
	public static void faceBlockPacket(BlockPos pos) {
		
		float oldYaw = Wrapper.mc.player.rotationYaw;
		float oldPitch = Wrapper.mc.player.rotationPitch;
		
		
		final double n = pos.getX() + 0.25;
   	 final double n2 = pos.getZ() + 0.25;
   	 final double var4 = n - Wrapper.mc.player.posX;
   	 final double var5 = n2 - Wrapper.mc.player.posZ;
   	 final double n3 = pos.getY() + 0.25;
   	 final double posY = Wrapper.mc.player.posY;
   	 final double var6 = n3 - (posY + Wrapper.mc.player.getEyeHeight());
        final double var7 = MathHelper.sqrt(var4 * var4 + var5 * var5);
        final float yaw = (float)(Math.atan2(var5, var4) * 180.0 / 3.141592653589793) - 90.0f;
        final float pitch = (float)(-(Math.atan2(var6, var7) * 180.0 / 3.141592653589793));
   	 final double posX = Wrapper.mc.player.posX;
        final double posY2 = Wrapper.mc.player.posY;
        final double posZ = Wrapper.mc.player.posZ;
        final float p_i45941_7_ = yaw;
        final float p_i45941_8_ = pitch;
        Wrapper.mc.player.rotationYawHead = p_i45941_7_;
        double oldPosX = Wrapper.mc.player.posX;
        double oldPosY = Wrapper.mc.player.posY;
        double oldPosZ = Wrapper.mc.player.posZ;
 
   	 Wrapper.mc.player.connection.sendPacket(new CPacketPlayer.PositionRotation(posX, posY2, posZ, p_i45941_7_, p_i45941_8_, Wrapper.mc.player.onGround));
	}
	
	
	public static void faceBlockClientSide(BlockPos pos) {
		final double n = pos.getX() + 0.25;
   	 final double n2 = pos.getZ() + 0.25;
   	 final double var4 = n - Wrapper.mc.player.posX;
   	 final double var5 = n2 - Wrapper.mc.player.posZ;
   	 final double n3 = pos.getY() + 0.25;
   	 final double posY = Wrapper.mc.player.posY;
   	 final double var6 = n3 - (posY + Wrapper.mc.player.getEyeHeight());
        final double var7 = MathHelper.sqrt(var4 * var4 + var5 * var5);
        final float yaw = (float)(Math.atan2(var5, var4) * 180.0 / 3.141592653589793) - 90.0f;
        final float pitch = (float)(-(Math.atan2(var6, var7) * 180.0 / 3.141592653589793));
   	 final double posX = Wrapper.mc.player.posX;
        final double posY2 = Wrapper.mc.player.posY;
        final double posZ = Wrapper.mc.player.posZ;
        final float p_i45941_7_ = yaw;
        final float p_i45941_8_ = pitch;
        Wrapper.mc.player.rotationYaw = p_i45941_7_;
        Wrapper.mc.player.rotationPitch = p_i45941_8_;
	}
	
public static void faceBlockPacket(int x, int y, int z) {
		
		float oldYaw = Wrapper.mc.player.rotationYaw;
		float oldPitch = Wrapper.mc.player.rotationPitch;
		
		
		final double n = x + 0.25;
   	 final double n2 = z + 0.25;
   	 final double var4 = n - Wrapper.mc.player.posX;
   	 final double var5 = n2 - Wrapper.mc.player.posZ;
   	 final double n3 = y + 0.25;
   	 final double posY = Wrapper.mc.player.posY;
   	 final double var6 = n3 - (posY + Wrapper.mc.player.getEyeHeight());
        final double var7 = MathHelper.sqrt(var4 * var4 + var5 * var5);
        final float yaw = (float)(Math.atan2(var5, var4) * 180.0 / 3.141592653589793) - 90.0f;
        final float pitch = (float)(-(Math.atan2(var6, var7) * 180.0 / 3.141592653589793));
   	 final double posX = Wrapper.mc.player.posX;
        final double posY2 = Wrapper.mc.player.posY;
        final double posZ = Wrapper.mc.player.posZ;
        final float p_i45941_7_ = yaw;
        final float p_i45941_8_ = pitch;
        Wrapper.mc.player.rotationYawHead = p_i45941_7_;
        double oldPosX = Wrapper.mc.player.posX;
        double oldPosY = Wrapper.mc.player.posY;
        double oldPosZ = Wrapper.mc.player.posZ;
 
   	 Wrapper.mc.player.connection.sendPacket(new CPacketPlayer.Rotation(yaw, pitch, Wrapper.mc.player.onGround));
   	Wrapper.mc.player.connection.sendPacket(new CPacketPlayer.Rotation(oldYaw, oldPitch, Wrapper.mc.player.onGround));
	}


public static Vec3d getClientLookVec()
{
	float f = WMath.cos(-Wrapper.mc.player.rotationYaw * 0.017453292F
		- (float)Math.PI);
	float f1 = WMath.sin(-Wrapper.mc.player.rotationYaw * 0.017453292F
		- (float)Math.PI);
	float f2 =
		-WMath.cos(-Wrapper.mc.player.rotationPitch * 0.017453292F);
	float f3 =
		WMath.sin(-Wrapper.mc.player.rotationPitch * 0.017453292F);
	return new Vec3d(f1 * f2, f3, f * f2);
}
	
}
