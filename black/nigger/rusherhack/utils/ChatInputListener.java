package black.nigger.rusherhack.utils;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.gui.ChatLine;
import net.minecraft.util.text.ITextComponent;

public interface ChatInputListener extends Listener
{
	public void onReceivedMessage(ChatInputEvent event);
	
	public static abstract class ChatInputEvent
		extends CancellableEvent<ChatInputListener>
	{
		private ITextComponent component;
		private List<ChatLine> chatLines;
		
		public ChatInputEvent(ITextComponent component,
			List<ChatLine> chatLines)
		{
			this.component = component;
			this.chatLines = chatLines;
		}
		
		public ITextComponent getComponent()
		{
			return component;
		}
		
		public void setComponent(ITextComponent component)
		{
			this.component = component;
		}
		
		public List<ChatLine> getChatLines()
		{
			return chatLines;
		}
		
		
		@Override
		public Class<ChatInputListener> getListenerType()
		{
			return ChatInputListener.class;
		}
	}
}