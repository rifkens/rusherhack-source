package black.nigger.rusherhack.utils;

import java.awt.Color;

import com.stringer.annotations.HideAccess;
import com.stringer.annotations.StringEncryption;

@HideAccess
@StringEncryption
public class ColorUtils{

    public static Color rainbowEffect(long offset, float fade){
        float hue = (float) (System.nanoTime() + offset) / 1.0E10F % 1.0F;
        long color = Long.parseLong(Integer.toHexString(Integer.valueOf(Color.HSBtoRGB(hue, 1.0F, 1.0F)).intValue()), 16);
        Color c = new Color((int) color);
        return new Color(c.getRed()/255.0F*fade, c.getGreen()/255.0F*fade, c.getBlue()/255.0F*fade, c.getAlpha()/255.0F);       
    }
    
    
    public static int rainbow(int delay) {
        double rainbowState = Math.ceil((System.currentTimeMillis() + delay) / 20.0);
        rainbowState %= 360;
        return Color.getHSBColor((float) (rainbowState / 360.0f), 0.8f, 0.7f).getRGB();
    }
    
    public static int getRainbowDown(int speed, int offset) {
        float hue = (float) ((System.currentTimeMillis() + offset) % speed);
        hue /= speed;
        return Color.getHSBColor(hue, 1.0F, 1.0F).getRGB();
    }
    
    public static int getRainbowUp(int speed, int offset) {
        float hue = (float) ((System.currentTimeMillis() - offset) % speed);
        hue /= speed;
        return Color.getHSBColor(hue, 1.0F, 1.0F).getRGB();
    }
    

}