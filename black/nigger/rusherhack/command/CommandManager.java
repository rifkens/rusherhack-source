package black.nigger.rusherhack.command;

import java.util.ArrayList;

import com.stringer.annotations.HideAccess;
import com.stringer.annotations.StringEncryption;

import black.nigger.rusherhack.command.commands.CmdAntiPacket;
import black.nigger.rusherhack.command.commands.CmdBind;
import black.nigger.rusherhack.command.commands.CmdCapes;
import black.nigger.rusherhack.command.commands.CmdCoords;
import black.nigger.rusherhack.command.commands.CmdCrash;
import black.nigger.rusherhack.command.commands.CmdDamage;
import black.nigger.rusherhack.command.commands.CmdHelp;
import black.nigger.rusherhack.command.commands.CmdLogo;
import black.nigger.rusherhack.command.commands.CmdMobOwner;
import black.nigger.rusherhack.command.commands.CmdNetherCoords;
import black.nigger.rusherhack.command.commands.CmdPeek;
import black.nigger.rusherhack.command.commands.CmdSave;
import black.nigger.rusherhack.command.commands.CmdToggle;
import black.nigger.rusherhack.command.commands.FriendCMD;
import black.nigger.rusherhack.command.commands.FriendRemoveCMD;

@HideAccess
@StringEncryption
public class CommandManager
{
	public static ArrayList<Command> commands = new ArrayList<Command>();

	public static char cmdPrefix = '.';

	public CommandManager()
	{
		addCommands();
	}

	public void addCommands()
	{
		commands.clear();
		commands.add(new CmdHelp());
		commands.add(new CmdDamage());
		commands.add(new CmdToggle());
		commands.add(new FriendCMD());
		commands.add(new FriendRemoveCMD());
		commands.add(new CmdPeek());
		commands.add(new CmdAntiPacket());
		commands.add(new CmdBind());
		commands.add(new CmdMobOwner());
		commands.add(new CmdLogo());
		commands.add(new CmdCapes());
		commands.add(new CmdCoords());
		commands.add(new CmdNetherCoords());
		commands.add(new CmdCrash());
		commands.add(new CmdSave());
	}
	
	public void runCommands(String s)
	{
		boolean commandResolved = false;
		String readString = s.trim();
		boolean hasArgs = readString.trim().contains(" ");
		String commandName = hasArgs ? readString.split(" ")[0] : readString.trim();
		String[] args = hasArgs ? readString.substring(commandName.length()).trim().split(" ") : new String[0];

		for(Command command: commands)
		{
			if(command.getCommand().trim().equalsIgnoreCase(commandName.trim())) 
			{
				command.runCommand(readString, args);
				commandResolved = true;
				break;
			}
		}

		if(!commandResolved)
		{
			Command.msg("Invalid command. Try help for a list of commands.");
		}
	}
}