package black.nigger.rusherhack.gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;
import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Lists;
import com.stringer.annotations.HideAccess;
import com.stringer.annotations.StringEncryption;

import black.nigger.rusherhack.Wwefan;
import black.nigger.rusherhack.events.EventPacketRecieve;
import black.nigger.rusherhack.utils.RenderingMethods;
import black.nigger.rusherhack.utils.Wrapper;
import net.minecraft.network.play.server.SPacketTimeUpdate;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@HideAccess
@StringEncryption
public class ServerInfo extends PinableFrame{
	
	public static final double MAX_TICKRATE = 20.D;
    public static final double MIN_TICKRATE = 0.D;

    public static final int MAXIMUM_SAMPLE_SIZE = 100;

    private static final ServerInfo INSTANCE = new ServerInfo();
    private static final TickRateData TICK_DATA = new TickRateData(MAXIMUM_SAMPLE_SIZE);

    public static ServerInfo getInstance() {
        return INSTANCE;
    }

    public static TickRateData getTickData() {
        return TICK_DATA;
    }

    private long timeLastTimeUpdate = -1;
	
	public ServerInfo() {
		super("Server Info", new String[] {}, 275);
		EventManager.register(this);
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	public void renderFrameText() {
		
		if(this.open) {
			
			 ArrayList<String> kek = new ArrayList<>();
	            kek.add("IP: " + (Wrapper.mc.isSingleplayer() ? "Single Player" : Wrapper.mc.getCurrentServerData().serverIP));
	            kek.add("Players: " + (Wrapper.mc.isSingleplayer() ? "IDK?" : TextFormatting.getTextWithoutFormattingCodes(Wrapper.mc.getCurrentServerData().populationInfo)));
	            kek.add(generateTickRateText());
	            kek.add("Ping: " + (Wrapper.mc.isSingleplayer() ? "0" : Wrapper.mc.getCurrentServerData().pingToServer));
	            kek.add("FPS: " + (Wrapper.mc.getDebugFPS()));
	            
	            this.setWidth(Wrapper.fr.getStringWidth(Wwefan.getLongestStringArray(kek)));
			
			RenderingMethods.drawBorderedRectReliantGui(this.x, this.y + this.barHeight + 1, this.x + this.getWidth(), this.y + this.barHeight + 3 + 50, 1,  new Color(25, 25, 25, 128).getRGB(), Color.gray.getRGB());
			Wrapper.fr.drawString("IP: " + (Wrapper.mc.isSingleplayer() ? "Single Player" : Wrapper.mc.getCurrentServerData().serverIP), x, this.y + this.barHeight + 3, -1);
			Wrapper.fr.drawString("Players: " + (Wrapper.mc.isSingleplayer() ? "IDK?" : TextFormatting.getTextWithoutFormattingCodes(Wrapper.mc.getCurrentServerData().populationInfo)), x, this.y + this.barHeight + 3 + 10, -1);
			Wrapper.fr.drawString(generateTickRateText(), x, this.y + this.barHeight + 3 + 20, -1);
			Wrapper.fr.drawString("Ping: " + (Wrapper.mc.isSingleplayer() ? "0" : Wrapper.mc.getCurrentServerData().pingToServer), x, this.y + this.barHeight + 3 + 30, -1);
			Wrapper.fr.drawString("FPS: " + (Wrapper.mc.getDebugFPS()), x, this.y + this.barHeight + 3 + 40, -1);
		}
		
	}
	
	private String generateTickRateText() {
        StringBuilder builder = new StringBuilder("Tick-rate: ");
        ServerInfo.TickRateData data = ServerInfo.getTickData();
        if(data.getSampleSize() <= 0) {
            builder.append("No tick data");
        } else {
            int factor = 25;
            int sections = data.getSampleSize() / factor;
            if (sections > 0) {
                    ServerInfo.TickRateData.CalculationData point = data.getPoint(100);
                    builder.append(String.format("%.2f", point.getAverage()));

            }
        }
        return builder.toString();
    }
	
	@SubscribeEvent
    public void onWorldLoad(WorldEvent.Load event) {
        timeLastTimeUpdate = -1;
        TICK_DATA.onWorldLoaded();
    }

    @EventTarget
    public void onPacketPreceived(EventPacketRecieve event) {
        if(event.getPacket() instanceof SPacketTimeUpdate) {
            if(timeLastTimeUpdate != -1) {
                TICK_DATA.onTimePacketIncoming(System.currentTimeMillis() - timeLastTimeUpdate);
            }
            timeLastTimeUpdate = System.currentTimeMillis();
        }
    }

    public static class TickRateData {
        private final CalculationData EMPTY_DATA = new CalculationData();

        private final Queue<Double> rates;
        private final List<CalculationData> data = Lists.newArrayList();

        private TickRateData(int maxSampleSize) {
            this.rates = EvictingQueue.create(maxSampleSize);
            for(int i = 0; i < maxSampleSize; i++) data.add(new CalculationData());
        }

        private void resetData() {
            for(CalculationData d : data) d.reset();
        }

        private void recalculate() {
            resetData();
            int size = 0;
            double total = 0.D;
            List<Double> in = Lists.newArrayList(rates);
            Collections.reverse(in);
            for(Double rate : in) {
                size++;
                total += rate;
                CalculationData d = data.get(size - 1);
                if(d != null) {
                    d.average = MathHelper.clamp(total / (double)(size), MIN_TICKRATE, MAX_TICKRATE);
                }
            }
        }

        public CalculationData getPoint(int point) {
            // clamp between existing data points and 0
            CalculationData d = data.get(Math.max(Math.min(getSampleSize() - 1, point - 1), 0));
            return d != null ? d : EMPTY_DATA;
        }

        public CalculationData getPoint() {
            return getPoint(getSampleSize() - 1);
        }

        public int getSampleSize() {
            return rates.size();
        }

        private void onTimePacketIncoming(long difference) {
            double timeElapsed = ((double)(difference) / 1000.D);
            rates.offer(MathHelper.clamp(MAX_TICKRATE / timeElapsed, MIN_TICKRATE, MAX_TICKRATE));
            // recalculate tick rate data
            recalculate();
        }

        private void onWorldLoaded() {
            rates.clear();
            resetData();
        }

        public class CalculationData {
            private double average = 0.D;

            public double getAverage() {
                return average;
            }

            public void reset() {
                average = 0.D;
            }
        }
    }
}