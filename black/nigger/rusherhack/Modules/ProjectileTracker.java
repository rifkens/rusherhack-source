package black.nigger.rusherhack.Modules;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;
import com.stringer.annotations.HideAccess;
import com.stringer.annotations.StringEncryption;

import black.nigger.rusherhack.Module.Category;
import black.nigger.rusherhack.Module.Module;
import black.nigger.rusherhack.command.Command;
import black.nigger.rusherhack.events.Render3DEvent;
import black.nigger.rusherhack.utils.ReflectionFields;
import black.nigger.rusherhack.utils.Wrapper;
import net.minecraft.util.math.MathHelper;

@HideAccess
@StringEncryption
public class ProjectileTracker extends Module {

	public ProjectileTracker() {
		super("ProjectileTracker", "ProjectileTracker", "", Keyboard.KEY_NONE, Category.Broken, -1);
	}

	@EventTarget
	public void Render(Render3DEvent event) {

		double arrowPosX = Wrapper.mc.player.lastTickPosX
				+ (Wrapper.mc.player.posX - Wrapper.mc.player.lastTickPosX)
						* ReflectionFields.getTimer().renderPartialTicks
				- MathHelper.cos((float) Math.toRadians(Wrapper.mc.player.rotationYaw)) * 0.16f;
		double arrowPosY = Wrapper.mc.player.lastTickPosY + (Wrapper.mc.player.posY - Wrapper.mc.player.lastTickPosY)
				* ReflectionFields.getTimer().renderPartialTicks + Wrapper.mc.player.getEyeHeight() - 0.1;
		double arrowPosZ = Wrapper.mc.player.lastTickPosZ
				+ (Wrapper.mc.player.posZ - Wrapper.mc.player.lastTickPosZ)
						* ReflectionFields.getTimer().renderPartialTicks
				- MathHelper.sin((float) Math.toRadians(Wrapper.mc.player.rotationYaw)) * 0.16f;

		float arrowMotionFactor = 1.0f;

		float yaw = (float) Math.toRadians(Wrapper.mc.player.rotationYaw);
		float pitch = (float) Math.toRadians(Wrapper.mc.player.rotationPitch);
		float arrowMotionX = -MathHelper.sin(yaw) * MathHelper.cos(pitch) * arrowMotionFactor;
		float arrowMotionY = -MathHelper.sin(pitch) * arrowMotionFactor;
		float arrowMotionZ = MathHelper.cos(yaw) * MathHelper.cos(pitch) * arrowMotionFactor;
		double arrowMotion = Math
				.sqrt(arrowMotionX * arrowMotionX + arrowMotionY * arrowMotionY + arrowMotionZ * arrowMotionZ);
		arrowMotionX /= (float) arrowMotion;
		arrowMotionY /= (float) arrowMotion;
		arrowMotionZ /= (float) arrowMotion;

		arrowMotionX *= 1.5;
		arrowMotionY *= 1.5;
		arrowMotionZ *= 1.5;

		arrowPosX += arrowMotionX * 0.1;
		arrowPosY += arrowMotionY * 0.1;
		arrowPosZ += arrowMotionZ * 0.1;
		arrowMotionX *= 0.999;
		arrowMotionY *= 0.999;
		arrowMotionZ *= 0.999;
		arrowMotionY -= (float) (0.05 * 0.1);

		Command.msg(arrowPosX + " " + arrowPosY + " " + arrowPosZ);

	}

	public void onEnable() {
		EventManager.register(this);
		super.onEnable();
	}

	public void onDisable() {
		EventManager.unregister(this);
		super.onDisable();
	}

}
