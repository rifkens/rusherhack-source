package black.nigger.rusherhack.Modules;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;
import com.stringer.annotations.HideAccess;
import com.stringer.annotations.StringEncryption;

import black.nigger.rusherhack.Module.Category;
import black.nigger.rusherhack.Module.Module;
import black.nigger.rusherhack.utils.ColourHolder;
import black.nigger.rusherhack.utils.Wrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.item.ItemStack;

@HideAccess
@StringEncryption
public class ArmorHUD extends Module {

	public ArmorHUD() {
		super("ArmorHUD", "ArmorHUD", "", Keyboard.KEY_NONE, Category.Render, -1);
	}

	@EventTarget
	public void onRender() {
		GlStateManager.enableTexture2D();

		ScaledResolution resolution = new ScaledResolution(Wrapper.mc);
		int i = resolution.getScaledWidth() / 2;
		int iteration = 0;
		int y = resolution.getScaledHeight() - 55 - (Wrapper.mc.player.isInWater() ? 10 : 0);
		for (ItemStack is : Wrapper.mc.player.inventory.armorInventory) {
			iteration++;
			if (is.isEmpty())
				continue;
			int x = i - 90 + (9 - iteration) * 20 + 2;
			GlStateManager.enableDepth();

			itemRender.zLevel = 200F;
			itemRender.renderItemAndEffectIntoGUI(is, x, y);
			itemRender.renderItemOverlayIntoGUI(Wrapper.mc.fontRenderer, is, x, y, "");
			itemRender.zLevel = 0F;

			GlStateManager.enableTexture2D();
			GlStateManager.disableLighting();
			GlStateManager.disableDepth();

			String s = is.getCount() > 1 ? is.getCount() + "" : "";
			Wrapper.mc.fontRenderer.drawStringWithShadow(s, x + 19 - 2 - Wrapper.mc.fontRenderer.getStringWidth(s),
					y + 9, 0xffffff);

			float green = ((float) is.getMaxDamage() - (float) is.getItemDamage()) / (float) is.getMaxDamage();
			float red = 1 - green;
			int dmg = 100 - (int) (red * 100);
			Wrapper.mc.fontRenderer.drawStringWithShadow(dmg + "",
					x + 8 - Wrapper.mc.fontRenderer.getStringWidth(dmg + "") / 2, y - 11,
					ColourHolder.toHex((int) (red * 255), (int) (green * 255), 0));

		}

		GlStateManager.enableDepth();
		GlStateManager.disableLighting();

	}

	private static RenderItem itemRender = Minecraft.getMinecraft().getRenderItem();

	public void onEnable() {
		EventManager.register(this);
		super.onEnable();
	}

	public void onDisable() {
		EventManager.unregister(this);
		super.onDisable();
	}
}
