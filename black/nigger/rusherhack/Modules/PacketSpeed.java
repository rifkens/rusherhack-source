package black.nigger.rusherhack.Modules;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;

import black.nigger.rusherhack.Module.Category;
import black.nigger.rusherhack.Module.Module;
import black.nigger.rusherhack.events.EventPacketRecieve;
import black.nigger.rusherhack.events.EventPacketSent;
import black.nigger.rusherhack.events.EventPreMotionUpdates;
import black.nigger.rusherhack.utils.Wrapper;
import net.minecraft.client.gui.GuiDownloadTerrain;
import net.minecraft.network.play.client.CPacketConfirmTeleport;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.network.play.server.SPacketPlayerPosLook;
import net.minecraft.util.math.BlockPos;

public class PacketSpeed extends Module {

	private List<CPacketPlayer> packets = new ArrayList<>();

	private boolean cancelIncoming = false;

	private int buffer = 0;

	private int teleportID = 0;

	public PacketSpeed() {
		super("PacketSpeed", "PacketSpeed", "", Keyboard.KEY_NONE, Category.Movement, -1);
	}

	public void onEnable() {

		buffer = 0;
		if (Wrapper.mc.player != null) {
			this.cancelIncoming = false;
			this.teleportID = 0;
			final CPacketPlayer.Position outOfBoundsPosition = new CPacketPlayer.Position(Wrapper.mc.player.posX,
					Wrapper.mc.player.posY - 420.69, Wrapper.mc.player.posZ, Wrapper.mc.player.onGround);
			packets.add(outOfBoundsPosition);
			Wrapper.mc.player.connection.sendPacket(outOfBoundsPosition);
		}

		EventManager.register(this);
		super.onEnable();
	}

	@EventTarget
	public void onMotion(EventPreMotionUpdates event) {
		if (this.teleportID <= 0) {
			final CPacketPlayer.Position outOfBoundsPosition = new CPacketPlayer.Position(Wrapper.mc.player.posX,
					Wrapper.mc.player.posY - 420.69, Wrapper.mc.player.posZ, Wrapper.mc.player.onGround);
			packets.add(outOfBoundsPosition);
			Wrapper.mc.player.connection.sendPacket(outOfBoundsPosition);
			return;
		}

		Wrapper.mc.player.setVelocity(0, 0, 0);

		if (Wrapper.mc.world.getCollisionBoxes(Wrapper.mc.player,
				Wrapper.mc.player.getEntityBoundingBox().expand(-0.0625d, 0, -0.0625d)).isEmpty()) {
			double ySpeed = 0;

			/*
			 * if (Wrapper.mc.gameSettings.keyBindJump.isKeyDown()) { ySpeed =
			 * Wrapper.mc.player.ticksExisted % 20 == 0 ? -0.04f : 0.062f; } else if
			 * (Wrapper.mc.gameSettings.keyBindSneak.isKeyDown()) { ySpeed = -0.062d; } else
			 * { ySpeed = Wrapper.mc.world.getCollisionBoxes(Wrapper.mc.player,
			 * Wrapper.mc.player.getEntityBoundingBox().expand(-0.0625d, -0.0625d,
			 * -0.0625d)).isEmpty() ? (Wrapper.mc.player.ticksExisted % 4 == 0) ? -0.04f :
			 * 0.0f : 0.0f; }
			 */

			double[] directionalSpeed = getDirectionalSpeed(0.25d);

			if (Wrapper.mc.gameSettings.keyBindJump.isKeyDown() || Wrapper.mc.gameSettings.keyBindSneak.isKeyDown()
					|| Wrapper.mc.gameSettings.keyBindForward.isKeyDown()
					|| Wrapper.mc.gameSettings.keyBindBack.isKeyDown()
					|| Wrapper.mc.gameSettings.keyBindRight.isKeyDown()
					|| Wrapper.mc.gameSettings.keyBindLeft.isKeyDown()) {
				if (directionalSpeed[0] != 0.0d || ySpeed != 0.0d || directionalSpeed[1] != 0.0d) {
					if (Wrapper.mc.player.movementInput.jump
							&& (Wrapper.mc.player.moveStrafing != 0 || Wrapper.mc.player.moveForward != 0)) {
						Wrapper.mc.player.setVelocity(0, 0, 0);
						move(0, 0, 0);
						// Wrapper.mc.player.setVelocity(0, ySpeed, 0);
						// move(0, ySpeed, 0);
						for (int i = 0; i <= 3; i++) {
							Wrapper.mc.player.setVelocity(0, ySpeed * i, 0);
							move(0, ySpeed * i, 0);
						}
					} else {
						if (Wrapper.mc.player.movementInput.jump) {
							Wrapper.mc.player.setVelocity(0, 0, 0);
							move(0, 0, 0);
							// Wrapper.mc.player.setVelocity(0, ySpeed, 0);
							// move(0, ySpeed, 0);
							for (int i = 0; i <= 3; i++) {
								Wrapper.mc.player.setVelocity(0, ySpeed * i, 0);
								move(0, ySpeed * i, 0);
							}
						} else {
							// Wrapper.mc.player.setVelocity(directionalSpeed[0], ySpeed,
							// directionalSpeed[1]);
							// move(directionalSpeed[0], ySpeed, directionalSpeed[1]);
							for (int i = 0; i <= 5; i++) {
								Wrapper.mc.player.setVelocity(directionalSpeed[0] * i, ySpeed, directionalSpeed[1] * i);
								move(directionalSpeed[0] * i, ySpeed, directionalSpeed[1] * i);
								move(directionalSpeed[0] * i, ySpeed, directionalSpeed[1] * i);
							}
						}
					}
				}
			} else {
				if (Wrapper.mc.world
						.getCollisionBoxes(Wrapper.mc.player,
								Wrapper.mc.player.getEntityBoundingBox().expand(-0.0625d, -0.0625d, -0.0625d))
						.isEmpty()) {
					Wrapper.mc.player.setVelocity(0, (Wrapper.mc.player.ticksExisted % 2 == 0) ? 0.04f : -0.04f, 0);
					move(0, (Wrapper.mc.player.ticksExisted % 2 == 0) ? 0.04f : -0.04f, 0);
				}
			}
		}

		event.setCancelled(true);
	}

	@EventTarget
	public void sendPacket(EventPacketSent event) {
		if (event.getPacket() instanceof CPacketPlayer && !(event.getPacket() instanceof CPacketPlayer.Position)) {
			event.setCancelled(true);
		}
		if (event.getPacket() instanceof CPacketPlayer) {
			final CPacketPlayer packetPlayer = (CPacketPlayer) event.getPacket();
			if (packets.contains(packetPlayer)) {
				packets.remove(packetPlayer);
				return;
			}
			event.setCancelled(true);
		}
	}

	@EventTarget
	public void eventReceivePacket(EventPacketRecieve event) {
		if (event.getPacket() instanceof SPacketPlayerPosLook && Wrapper.mc.player != null
				&& Wrapper.mc.player.isEntityAlive()
				&& Wrapper.mc.world.isBlockLoaded(
						new BlockPos(Wrapper.mc.player.posX, Wrapper.mc.player.posY, Wrapper.mc.player.posZ))
				&& !(Wrapper.mc.currentScreen instanceof GuiDownloadTerrain)) {
			SPacketPlayerPosLook packetPlayerPosLook = (SPacketPlayerPosLook) event.getPacket();

			if (this.teleportID <= 0) {
				this.teleportID = packetPlayerPosLook.getTeleportId();
			}

			if (this.buffer > 0 && this.teleportID > 0) {
				this.buffer = this.buffer - 1;

				event.setCancelled(true);
			}
		}
	}

	public void onDisable() {
		EventManager.unregister(this);
		super.onDisable();
	}

	private void move(double x, double y, double z) {
		final CPacketPlayer.Position newPosition = new CPacketPlayer.Position(Wrapper.mc.player.posX + x,
				Wrapper.mc.player.posY, Wrapper.mc.player.posZ + z, Wrapper.mc.player.onGround);
		packets.add(newPosition);
		Wrapper.mc.player.connection.sendPacket(newPosition);

		final CPacketPlayer.Position outOfBoundsPosition = new CPacketPlayer.Position(Wrapper.mc.player.posX + x,
				Wrapper.mc.player.posY - 420.69, Wrapper.mc.player.posZ + z, Wrapper.mc.player.onGround);
		packets.add(outOfBoundsPosition);
		Wrapper.mc.player.connection.sendPacket(outOfBoundsPosition);

		++this.buffer;
		++this.teleportID;

		Wrapper.mc.player.connection.sendPacket(new CPacketConfirmTeleport(this.teleportID));
		Wrapper.mc.player.connection.sendPacket(new CPacketConfirmTeleport(this.teleportID));
	}

	public double[] getDirectionalSpeed(double speed) {
		float moveForward = Wrapper.mc.player.movementInput.moveForward;
		float moveStrafe = Wrapper.mc.player.movementInput.moveStrafe;
		float rotationYaw = Wrapper.mc.player.prevRotationYaw
				+ (Wrapper.mc.player.rotationYaw - Wrapper.mc.player.prevRotationYaw)
						* Wrapper.mc.getRenderPartialTicks();

		if (moveForward != 0) {
			if (moveStrafe > 0) {
				rotationYaw += (moveForward > 0 ? -45 : 45);
			} else if (moveStrafe < 0) {
				rotationYaw += (moveForward > 0 ? 45 : -45);
			}
			moveStrafe = 0;
			if (moveForward > 0) {
				moveForward = 1;
			} else if (moveForward < 0) {
				moveForward = -1;
			}
		}

		double posX = (moveForward * speed * Math.cos(Math.toRadians(rotationYaw + 90))
				+ moveStrafe * speed * Math.sin(Math.toRadians(rotationYaw + 90)));
		double posZ = (moveForward * speed * Math.sin(Math.toRadians(rotationYaw + 90))
				- moveStrafe * speed * Math.cos(Math.toRadians(rotationYaw + 90)));

		return new double[] { posX, posZ };
	}

}