package black.nigger.rusherhack.Modules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.input.Keyboard;

import com.stringer.annotations.HideAccess;
import com.stringer.annotations.StringEncryption;

import black.nigger.rusherhack.Module.Category;
import black.nigger.rusherhack.Module.Module;
import black.nigger.rusherhack.utils.Wrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetworkPlayerInfo;

@HideAccess
@StringEncryption
public class Welcomer extends Module {

	static ArrayList<NetworkPlayerInfo> playerMap;
	static int cachePlayerCount;
	boolean isOnServer;
	public static ArrayList<String> greets;
	public static ArrayList<String> goodbyes;

	public static ArrayList<String> joinMessages = new ArrayList<>();
	public static ArrayList<String> leaveMessages = new ArrayList<>();

	static {
		playerMap = new ArrayList<NetworkPlayerInfo>();
		greets = new ArrayList<String>();
		goodbyes = new ArrayList<String>();
	}

	public Welcomer() {
		super("Welcomer", "Welcomer", "Sends a message when a player joins/leaves", Keyboard.KEY_NONE, Category.Spam,
				0xFF1A7CA6);
	}

	public void onUpdate() {
		if (!this.getState()) {
			return;
		}

		if (Wrapper.mc.player != null) {
			if (Wrapper.mc.player.ticksExisted % 10 == 0) {
				checkPlayers();
			} else if (Wrapper.mc.isSingleplayer()) {
				this.toggleModule();
			}
		}

	}

	public void onEnable() {

		onJoinServer();
		super.onEnable();
	}

	private void checkPlayers() {
		final ArrayList<NetworkPlayerInfo> infoMap = new ArrayList<NetworkPlayerInfo>(
				Minecraft.getMinecraft().getConnection().getPlayerInfoMap());
		final int currentPlayerCount = infoMap.size();
		if (currentPlayerCount != cachePlayerCount) {
			final ArrayList<NetworkPlayerInfo> currentInfoMap = (ArrayList<NetworkPlayerInfo>) infoMap.clone();
			currentInfoMap.removeAll(playerMap);
			if (currentInfoMap.size() > 5) {
				cachePlayerCount = playerMap.size();
				this.onJoinServer();
				return;
			}
			final ArrayList<NetworkPlayerInfo> playerMapClone = (ArrayList<NetworkPlayerInfo>) playerMap.clone();
			playerMapClone.removeAll(infoMap);
			for (final NetworkPlayerInfo npi : currentInfoMap) {
				this.playerJoined(npi);
			}
			for (final NetworkPlayerInfo npi : playerMapClone) {
				this.playerLeft(npi);
			}
			cachePlayerCount = playerMap.size();
			this.onJoinServer();
		}
	}

	private void onJoinServer() {
		playerMap = new ArrayList<NetworkPlayerInfo>(Minecraft.getMinecraft().getConnection().getPlayerInfoMap());
		cachePlayerCount = playerMap.size();
		this.isOnServer = true;
	}

	protected void playerJoined(final NetworkPlayerInfo playerInfo) {
		try {
			joinMessages = Wrapper.getFileManager().LoadJoinMessages(playerInfo);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Wrapper.mc.player.sendChatMessage(joinMessages.get(new Random().nextInt(joinMessages.size())));
	}

	protected void playerLeft(final NetworkPlayerInfo playerInfo) {
		try {
			leaveMessages = Wrapper.getFileManager().LoadLeaveMessages(playerInfo);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Wrapper.mc.player.sendChatMessage(leaveMessages.get(new Random().nextInt(leaveMessages.size())));
	}
}