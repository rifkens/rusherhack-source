package black.nigger.rusherhack.Modules;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;
import com.stringer.annotations.HideAccess;
import com.stringer.annotations.StringEncryption;

import black.nigger.rusherhack.Module.Category;
import black.nigger.rusherhack.Module.Module;
import black.nigger.rusherhack.events.MoveEvent;
import black.nigger.rusherhack.utils.ReflectionFields;
import black.nigger.rusherhack.utils.Wrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.init.MobEffects;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.util.MovementInput;

@HideAccess
@StringEncryption
public class Strafe extends Module {

	private static float speed;
	private double speed1;
	private int stage;
	private boolean disabling;
	private boolean stopMotionUntilNext;
	private double moveSpeed;
	private boolean spedUp;
	public static boolean canStep;
	private double lastDist;
	public static double yOffset;
	private boolean cancel;

	public Strafe() {
		super("Strafe", "Strafe", "", Keyboard.KEY_NONE, Category.Movement, -1);
		speed = 0.08f;
	}

	@EventTarget
	public void onEvent(MoveEvent event) {
		if (!this.getState()) {
			return;
		}
		if (Wrapper.mc.player.isAirBorne) {
			this.moveSpeed = Math.max(this.moveSpeed, this.getBaseMoveSpeed());
			MovementInput movementInput = Wrapper.mc.player.movementInput;
			float forward = movementInput.moveForward;
			float strafe = movementInput.moveStrafe;
			float yaw = Minecraft.getMinecraft().player.rotationYaw;
			if (forward == 0.0f && strafe == 0.0f) {
				event.x = 0.0;
				event.z = 0.0;
			} else if (forward != 0.0f) {
				if (strafe >= 1.0f) {
					yaw += (float) (forward > 0.0f ? -45 : 45);
					strafe = 0.0f;
				} else if (strafe <= -1.0f) {
					yaw += (float) (forward > 0.0f ? 45 : -45);
					strafe = 0.0f;
				}
				if (forward > 0.0f) {
					forward = 1.0f;
				} else if (forward < 0.0f) {
					forward = -1.0f;
				}
			}
			double mx2 = Math.cos(Math.toRadians(yaw + 90.0f));
			double mz2 = Math.sin(Math.toRadians(yaw + 90.0f));
			double motionX = (double) forward * this.moveSpeed * mx2 + (double) strafe * this.moveSpeed * mz2;
			double motionZ = (double) forward * this.moveSpeed * mz2 - (double) strafe * this.moveSpeed * mx2;
			event.x = (double) forward * this.moveSpeed * mx2 + (double) strafe * this.moveSpeed * mz2;
			event.z = (double) forward * this.moveSpeed * mz2 - (double) strafe * this.moveSpeed * mx2;
			Wrapper.mc.player.connection.sendPacket(new CPacketPlayer(false));
			Wrapper.mc.player.connection.sendPacket(new CPacketPlayer(true));
		} else {
			Wrapper.mc.player.jumpMovementFactor = 0.02f;
			ReflectionFields.getTimer().elapsedTicks = (int) 1.0F;
		}
	}

	public void onUpdate() {
		if (!this.getState()) {
			return;
		}

		double xDist = Wrapper.mc.player.posX - Wrapper.mc.player.prevPosX;
		double zDist = Wrapper.mc.player.posZ - Wrapper.mc.player.prevPosZ;
		this.lastDist = Math.sqrt(xDist * xDist + zDist * zDist);

	}

	public void onEnable() {
		EventManager.register(this);
		super.onEnable();
		ReflectionFields.getTimer().elapsedTicks = (int) 1.0F;
		ReflectionFields.setSpeedInAir(Wrapper.mc.player, 0.04F);
		this.cancel = false;
		this.stage = 1;
		double d2 = this.moveSpeed = Wrapper.mc.player == null ? 0.2873 : this.getBaseMoveSpeed();
	}

	public void onDisable() {
		EventManager.unregister(this);
		super.onDisable();
		ReflectionFields.getTimer().elapsedTicks = (int) 1.0F;
		this.moveSpeed = this.getBaseMoveSpeed();
		yOffset = 0.0;
		this.stage = 0;
		this.disabling = false;
		this.cancel = true;
		ReflectionFields.setSpeedInAir(Wrapper.mc.player, 0.02F);
	}

	private double getBaseMoveSpeed() {
		double baseSpeed = 0.2873;
		if (Wrapper.mc.player.isPotionActive(MobEffects.SPEED)) {
			int amplifier = Wrapper.mc.player.getActivePotionEffect(MobEffects.SPEED).getAmplifier();
			baseSpeed *= 1.0 + 0.2 * (double) (amplifier + 1);
		}
		return baseSpeed;
	}

}
