package black.nigger.rusherhack.Modules;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.stringer.annotations.HideAccess;
import com.stringer.annotations.StringEncryption;

import black.nigger.rusherhack.Module.Category;
import black.nigger.rusherhack.Module.Module;
import black.nigger.rusherhack.utils.ReflectionFields;
import black.nigger.rusherhack.utils.WBlock;
import black.nigger.rusherhack.utils.WItem;
import black.nigger.rusherhack.utils.WPlayerController;
import black.nigger.rusherhack.utils.Wrapper;
import net.minecraft.block.BlockContainer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.init.Items;
import net.minecraft.item.ItemSoup;
import net.minecraft.item.ItemStack;

@HideAccess
@StringEncryption
public class AutoSoup extends Module {

	public AutoSoup() {
		super("AutoSoup", "AutoSoup", "", Keyboard.KEY_NONE, Category.Combat, -1);
	}
	
	private int oldSlot = -1;
	
	public void onUpdate() {
		if(!this.getState()) {
			return;
		}
		
		// sort empty bowls
		for(int i = 0; i < 36; i++)
		{
			// filter out non-bowl items and empty bowl slot
			ItemStack stack =
				Wrapper.mc.player.inventory.getStackInSlot(i);
			if(stack == null || stack.getItem() != Items.BOWL || i == 9)
				continue;
			
			// check if empty bowl slot contains a non-bowl item
			ItemStack emptyBowlStack =
				Wrapper.mc.player.inventory.getStackInSlot(9);
			boolean swap = !WItem.isNullOrEmpty(emptyBowlStack)
				&& emptyBowlStack.getItem() != Items.BOWL;
			
			// place bowl in empty bowl slot
			WPlayerController.windowClick_PICKUP(i < 9 ? 36 + i : i);
			WPlayerController.windowClick_PICKUP(9);
			
			// place non-bowl item from empty bowl slot in current slot
			if(swap)
				WPlayerController.windowClick_PICKUP(i < 9 ? 36 + i : i);
		}
		
		// search soup in hotbar
		int soupInHotbar = findSoup(0, 9);
		
		// check if any soup was found
		if(soupInHotbar != -1)
		{
			// check if player should eat soup
			if(!shouldEatSoup())
			{
				stopIfEating();
				return;
			}
			
			// save old slot
			if(oldSlot == -1)
				oldSlot = Wrapper.mc.player.inventory.currentItem;
			
			// set slot
			Wrapper.mc.player.inventory.currentItem = soupInHotbar;
			
			// eat soup
			ReflectionFields.setPressed(Wrapper.mc.gameSettings.keyBindUseItem, false);
			WPlayerController.processRightClick();
			
			return;
		}
		
		stopIfEating();
		
		// search soup in inventory
		int soupInInventory = findSoup(9, 36);
		
		// move soup in inventory to hotbar
		if(soupInInventory != -1)
			WPlayerController.windowClick_QUICK_MOVE(soupInInventory);
           
    }
	
	private int findSoup(int startSlot, int endSlot)
	{
		for(int i = startSlot; i < endSlot; i++)
		{
			ItemStack stack =
				Wrapper.mc.player.inventory.getStackInSlot(i);
			
			if(stack != null && stack.getItem() instanceof ItemSoup)
				return i;
		}
		
		return -1;
	}
	
	private boolean shouldEatSoup()
	{
		// check health
		if(Wrapper.mc.player.getHealth() > 6.5 * 2F)
			return false;
		
		// check screen
		if(Wrapper.mc.currentScreen != null)
			return false;
		
		// check for clickable objects
		if(Wrapper.mc.currentScreen == null && Wrapper.mc.objectMouseOver != null)
		{
			// clickable entities
			Entity entity = Wrapper.mc.objectMouseOver.entityHit;
			if(entity instanceof EntityVillager
				|| entity instanceof EntityTameable)
				return false;
			
			// clickable blocks
			if(Wrapper.mc.objectMouseOver.getBlockPos() != null && WBlock.getBlock(
					Wrapper.mc.objectMouseOver.getBlockPos()) instanceof BlockContainer)
				return false;
		}
		
		return true;
	}
	
	private void stopIfEating()
	{
		// check if eating
		if(oldSlot == -1)
			return;
		
		// stop eating
		ReflectionFields.setPressed(Wrapper.mc.gameSettings.keyBindUseItem, false);
		
		// reset slot
		Wrapper.mc.player.inventory.currentItem = oldSlot;
		oldSlot = -1;
	}

	public void onEnable() {
		EventManager.register(this);
		super.onEnable();
	}

	public void onDisable() {
		EventManager.unregister(this);
		super.onDisable();
	}
}

