package black.nigger.rusherhack.events;

import black.nigger.rusherhack.utils.Event;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.EnumSkyBlock;
import net.minecraftforge.fml.common.eventhandler.Cancelable;

@Cancelable
public class WorldCheckLightForEvent extends Event {
  private final EnumSkyBlock enumSkyBlock;
  private final BlockPos pos;

  public WorldCheckLightForEvent(EnumSkyBlock enumSkyBlock, BlockPos pos) {
    this.enumSkyBlock = enumSkyBlock;
    this.pos = pos;
  }

  public EnumSkyBlock getEnumSkyBlock() {
    return enumSkyBlock;
  }

  public BlockPos getPos() {
    return pos;
  }
}