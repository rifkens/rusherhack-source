package black.nigger.rusherhack.mixin.mixins;

import java.util.List;

import javax.annotation.Nullable;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.darkmagician6.eventapi.EventManager;

import black.nigger.rusherhack.Wwefan;
import black.nigger.rusherhack.Modules.Freecam;
import black.nigger.rusherhack.Modules.Xray;
import black.nigger.rusherhack.events.AddCollisionBoxToListEvent;
import black.nigger.rusherhack.utils.XrayUtils;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

@Mixin(value = Block.class, priority = 9998)
public class MixinBlock {
	
	@Inject(method = "addCollisionBoxToList", at = @At("HEAD"), cancellable = true)
    private void CollistionList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, @Nullable Entity entityIn, boolean isActualState, CallbackInfo callback) {
		AddCollisionBoxToListEvent collisionBoxEvent = new AddCollisionBoxToListEvent(state, worldIn, pos, entityBox, collidingBoxes, entityIn, isActualState);
        EventManager.call(collisionBoxEvent);
        if (collisionBoxEvent.isCancelled()) {
          callback.cancel();
        }
    }
	
	@Inject(method = "getCollisionBoundingBox", at = @At("HEAD"), cancellable = true)
    private void getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos, CallbackInfoReturnable<AxisAlignedBB> callback) {
		try
        {
    		if(Wwefan.moduleManger.getModule(Freecam.class).getState()) {
        		callback.setReturnValue(Block.NULL_AABB);
        	}
        }
        catch (Exception localException) {
        	
        }    
    }
	@Inject(method = "isFullCube", at = @At("HEAD"), cancellable = true)
    public void isFullCube(IBlockState state, CallbackInfoReturnable<Boolean> callback) {
        try
        {
            
            if (Wwefan.moduleManger.getModule(Xray.class).getState()) {
                callback.setReturnValue(Xray.shouldXray(Block.class.cast(this)));
                callback.cancel();
              }
        }catch (Exception e) {
            
        }
    }
    
	
    @Inject(method = "shouldSideBeRendered", at = @At("HEAD"), cancellable = true)
    public void shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side, CallbackInfoReturnable<Boolean> callback) {
        try
        {
            if (Wwefan.moduleManger.getModule(Xray.class).getState()) {
                callback.setReturnValue(XrayUtils.isXrayBlock(Block.class.cast(this)));
              }
        }catch (Exception e) {
            
        }
    }
    
    @Inject(method = "isOpaqueCube", at = @At("HEAD"), cancellable = true)
    public void isOpaqueCube(IBlockState state, CallbackInfoReturnable<Boolean> callback) {
        try
        {
            if (Wwefan.moduleManger.getModule(Xray.class).getState()) {
                callback.setReturnValue(Xray.shouldXray(Block.class.cast(this)));
              }
        }catch (Exception e) {
            
        }
    }
	
}
