package black.nigger.rusherhack.mixin.mixins;

import java.io.IOException;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

import black.nigger.rusherhack.Wwefan;
import black.nigger.rusherhack.Modules.Colors;
import io.netty.buffer.Unpooled;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiRepair;
import net.minecraft.client.gui.GuiScreenBook;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.client.CPacketCustomPayload;

@Mixin(value = GuiScreenBook.class, priority = 9998)
public class MixinGuiScreenBook {
	
	@Final
	@Shadow
	private EntityPlayer editingPlayer;
	
	@Final
	@Shadow
	private ItemStack book;
	
	@Final
	@Shadow
	private boolean bookIsUnsigned;
	
	@Shadow
	private boolean bookIsModified;
	
	@Shadow
	private NBTTagList bookPages;
	
	@Shadow
	private String bookTitle = "";
	
	@Overwrite
	private void sendBookToServer(boolean publish) throws IOException
    {
        if (this.bookIsUnsigned && this.bookIsModified)
        {
            if (this.bookPages != null)
            {
                while (this.bookPages.tagCount() > 1)
                {
                    String s = this.bookPages.getStringTagAt(this.bookPages.tagCount() - 1);

                    if (!s.isEmpty())
                    {
                        break;
                    }

                    this.bookPages.removeTag(this.bookPages.tagCount() - 1);
                }

                if (this.book.hasTagCompound())
                {
                    NBTTagCompound nbttagcompound = this.book.getTagCompound();
                    nbttagcompound.setTag("pages", this.bookPages);
                }
                else
                {
                    this.book.setTagInfo("pages", this.bookPages);
                }

                String s1 = "MC|BEdit";

                if (publish)
                {
                    s1 = "MC|BSign";
                    if(Wwefan.moduleManger.getModule(Colors.class).getState()) {
                    this.book.setTagInfo("author", new NBTTagString(this.editingPlayer.getName().replaceAll("&", "\u00A7\u00A7\u00A7")));
                    this.book.setTagInfo("title", new NBTTagString(this.bookTitle.trim().replaceAll("&", "\u00A7\u00A7\u00A7")));
                    }else {
                    	this.book.setTagInfo("author", new NBTTagString(this.editingPlayer.getName()));
                        this.book.setTagInfo("title", new NBTTagString(this.bookTitle.trim()));
                    }
                }

                PacketBuffer packetbuffer = new PacketBuffer(Unpooled.buffer());
                packetbuffer.writeItemStack(this.book);
                Minecraft.getMinecraft().getConnection().sendPacket(new CPacketCustomPayload(s1, packetbuffer));
            }
        }
    }
	
}
