package black.nigger.rusherhack.mixin.mixins;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.darkmagician6.eventapi.EventManager;

import black.nigger.rusherhack.Wwefan;
import black.nigger.rusherhack.Modules.AntiFog;
import black.nigger.rusherhack.Modules.AntiHurtCam;
import black.nigger.rusherhack.Modules.CameraClip;
import black.nigger.rusherhack.Modules.Freecam;
import black.nigger.rusherhack.events.Render3DEvent;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

@Mixin(value = EntityRenderer.class, priority = 9998)
public class MixinEntityRenderer {
	 @Inject(method = "renderWorldPass", at = @At(value = "FIELD", target = "Lnet/minecraft/client/renderer/EntityRenderer;renderHand:Z"))
	    private void renderWorldPassPre(int pass, float partialTicks, long finishTimeNano, CallbackInfo callbackInfo) {

		 Wwefan.onRender();
     	Render3DEvent event = new Render3DEvent(partialTicks);
         EventManager.call(event);
	    }
	 
	    @Inject(method = "setupFog", at = @At(value = "HEAD"), cancellable = true)
	    public void setupFog(int startCoords, float partialTicks, CallbackInfo callbackInfo) {
	        if (Wwefan.moduleManger.getModule(AntiFog.class).getState())
	            callbackInfo.cancel();
	    }

	    @Redirect(method = "setupFog", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/renderer/ActiveRenderInfo;getBlockStateAtEntityViewpoint(Lnet/minecraft/world/World;Lnet/minecraft/entity/Entity;F)Lnet/minecraft/block/state/IBlockState;"))
	    public IBlockState getBlockStateAtEntityViewpoint(World worldIn, Entity entityIn, float p_186703_2_) {
	        if (Wwefan.moduleManger.getModule(AntiFog.class).getState()) return (IBlockState) Blocks.AIR;
	        return ActiveRenderInfo.getBlockStateAtEntityViewpoint(worldIn, entityIn, p_186703_2_);
	    }
	 
	 @Inject(method = "hurtCameraEffect", at = @At("HEAD"), cancellable = true)
	 public void hurtCameraEffect(float partialTicks, CallbackInfo callback) {
		 if(Wwefan.moduleManger.getModule(AntiHurtCam.class).getState()) {
			 callback.cancel();
	    	}
	 }
	 
	 @Redirect(method = "orientCamera", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/multiplayer/WorldClient;rayTraceBlocks(Lnet/minecraft/util/math/Vec3d;Lnet/minecraft/util/math/Vec3d;)Lnet/minecraft/util/math/RayTraceResult;"))
	    public RayTraceResult rayTraceBlocks(WorldClient world, Vec3d start, Vec3d end) {
	        if (Wwefan.moduleManger.getModule(CameraClip.class).getState()) {
	            return null;
	        }else {
	            return world.rayTraceBlocks(start, end);
	        }
	    }
	 
	 @ModifyArg(method = "renderWorldPass(IFJ)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/renderer/RenderGlobal;setupTerrain(Lnet/minecraft/entity/Entity;DLnet/minecraft/client/renderer/culling/ICamera;IZ)V"))
	    public boolean isPlayerSpectator(boolean playerSpectator) {
	        return playerSpectator || Wwefan.moduleManger.getModule(Freecam.class).getState();
	    }
}